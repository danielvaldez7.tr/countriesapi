-- Para garantizar los cambios por unidad puede usar un commit() y rollback()
create table countries(
	country_cve int not null,
	country_name varchar not null,
	population int
);

alter table countries
add constraint pk_countries_id
primary key (country_cve);


create table states(
	state_cve int not null,
	country_cve int not null,
	state_name varchar not null,
	population int
);

alter table states
add constraint fk_country_cve_countries
foreign key (country_cve)
references countries(country_cve);
