package com.example.servicesexample.state.controller;


import com.example.servicesexample.state.model.State;
import com.example.servicesexample.state.service.StateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.smartcardio.CardTerminals;
import java.util.List;

@RestController
public class StateController {
    private StateService service;

    public StateController(StateService service) {
        this.service = service;
    }

//    @GetMapping("/Country/{id}/states")
    @GetMapping("/states")
    public ResponseEntity<List<State>> getStates() {
        List<State> stateList = service.getAllStates();
        return new ResponseEntity(stateList, HttpStatus.OK);
    }

    @PostMapping("/states")
    public ResponseEntity<State> createStata(@RequestBody State newState) {
        State stateSaved = service.saveState(newState);
        return new ResponseEntity(stateSaved, HttpStatus.CREATED);
    }
}
