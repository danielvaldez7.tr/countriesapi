package com.example.servicesexample.state.model;

import com.example.servicesexample.country.model.Country;

import javax.persistence.*;

@Entity
@Table(name="states")
public class State {
    @Id
    @Column(name="state_cve")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="state_name")
    private String name;
    private int population;
    @Column(name="country_cve")
    private int country_cve;

//    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "country_id", nullable = false, updatable = false)
//    private com.example.servicesexample.country.model.Country Country;

    public State() {
    }

    public State(String name, int population, int country_cve) {
        this.name = name;
        this.population = population;
        this.country_cve = country_cve;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getCountry_cve() {
        return country_cve;
    }

    public void setCountry_cve(int country_cve) {
        this.country_cve = country_cve;
    }
}
