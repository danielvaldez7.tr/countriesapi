package com.example.servicesexample.state.service;

import com.example.servicesexample.state.model.State;

import java.util.List;

public interface IStateService {
    List<State> getAllStates();
    State saveState(State state);
    State getStateById(Long id);
    State replaceState(State state, Long id);
    void deleteState(State state);
}
