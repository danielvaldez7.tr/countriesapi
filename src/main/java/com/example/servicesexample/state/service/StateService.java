package com.example.servicesexample.state.service;


import com.example.servicesexample.state.model.State;
import com.example.servicesexample.state.repository.StateRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StateService implements IStateService{
    private StateRepository repository;

    public StateService(StateRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<State> getAllStates() {
        return repository.findAll();
    }

    @Override
    public State saveState(State newState) {
        return  repository.save(newState);
    }

    @Override
    public State getStateById(Long id) {
        return null;
    }

    @Override
    public State replaceState(State state, Long id) {
        return null;
    }

    @Override
    public void deleteState(State state) {

    }
}
