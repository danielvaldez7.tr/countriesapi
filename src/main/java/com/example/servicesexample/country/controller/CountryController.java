package com.example.servicesexample.country.controller;

import com.example.servicesexample.country.model.Country;
import com.example.servicesexample.country.service.CountryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CountryController {
    private CountryService countryService;

    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping(value = "/")
    String sendGretting() {
        return "Hola Mundo desde spring";
    }

    @GetMapping(value = "/countries")
    ResponseEntity<?> getCountries() {
        return new ResponseEntity<List<Country>>(
                countryService.getAllCountries(),
                HttpStatus.OK);
    }

    @PostMapping(value = "/countries")
    ResponseEntity<?> create(@RequestBody Country newCountry) {
        Country countrySaved = countryService.saveCountry(newCountry);
        return new ResponseEntity(countrySaved, HttpStatus.CREATED);
    }

    @GetMapping(value = "/countries/{id}")
    ResponseEntity<?> getCountries(@PathVariable Long id) {
        Country country = countryService.getCountryById(id);
        return new ResponseEntity(country, HttpStatus.OK);
    }

    @PutMapping("/countries/{id}")
    ResponseEntity<?> updateCountry(@RequestBody Country updateCountry, @PathVariable Long id){
        Country CountryUpdated = countryService.replaceCountry(updateCountry, id);
        return new ResponseEntity(CountryUpdated, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/countries/{id}")
    ResponseEntity<?> deleteCountry(@PathVariable Long id){
        Country countryDelete = countryService.getCountryById(id);
        countryService.deleteCountry(countryDelete);
        return new ResponseEntity("Country with id: " +  id + " has been deleted", HttpStatus.OK);
    }
}