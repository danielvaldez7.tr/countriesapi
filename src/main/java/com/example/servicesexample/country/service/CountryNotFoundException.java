package com.example.servicesexample.country.service;

public class CountryNotFoundException extends RuntimeException {
    public CountryNotFoundException(Long id) {
        super("Exception Country with id: " + id + " Not found");
    }
}
