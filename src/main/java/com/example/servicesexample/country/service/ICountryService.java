package com.example.servicesexample.country.service;

import com.example.servicesexample.country.model.Country;

import java.util.List;

public interface ICountryService {
    List<Country> getAllCountries();
    Country saveCountry(Country country);
    Country getCountryById(Long id);
    Country replaceCountry(Country country, Long id);
    void deleteCountry(Country country);
}
