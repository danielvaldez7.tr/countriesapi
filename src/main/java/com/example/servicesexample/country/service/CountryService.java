package com.example.servicesexample.country.service;

import com.example.servicesexample.country.model.Country;
import com.example.servicesexample.country.repository.CountryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CountryService implements ICountryService {
    private CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    @Override
    public Country saveCountry(Country country) {
        return countryRepository.save(country);
    }


    @Override
    public Country getCountryById(Long id) {
        return countryRepository.findById(id)
                .orElseThrow(()-> new CountryNotFoundException(id));
    }

    @Override
    public Country replaceCountry(Country updateCountry, Long id) {
        return countryRepository.findById(id)
                .map(country-> {
                    country.setName(updateCountry.getName());
                    country.setPopulation(updateCountry.getPopulation());
                    return countryRepository.save(country);
                })
                .orElseGet(()-> {
                    updateCountry.setId(id);
                    return countryRepository.save(updateCountry);
                });
    }

    @Override
    public void deleteCountry(Country country) {
        countryRepository.delete(country);
    }
}
