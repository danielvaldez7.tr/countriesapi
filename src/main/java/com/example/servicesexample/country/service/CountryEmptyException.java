package com.example.servicesexample.country.service;

public class CountryEmptyException extends RuntimeException{
    public CountryEmptyException(String message) {
        super(message);
    }
}
