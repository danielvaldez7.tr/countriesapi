package com.example.servicesexample.country.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CountryEmptyAdvice {

    @ResponseBody
    @ExceptionHandler(CountryEmptyException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String countryEmptyHandler(CountryEmptyException ex) {
        return ex.getMessage();
    }
}
