package com.example.servicesexample.country.repository;

import com.example.servicesexample.country.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository  extends JpaRepository<Country, Long> {
}
